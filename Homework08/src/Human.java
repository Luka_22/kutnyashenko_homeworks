public class Human implements Comparable<Human> {

    private String name;
    private double weight;

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight() {
        this.weight = randomWeight();
    }

    public double randomWeight() {
        int minWeight = 40;
        int maxWeight = 140;
        int max = maxWeight - minWeight;
        return (Math.random() * max) + minWeight;
    }

    @Override
    public int compareTo(Human o) {
        return (int) (this.weight - o.weight);
    }
}
