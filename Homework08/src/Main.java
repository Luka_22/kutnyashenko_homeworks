import java.util.Arrays;

public class Main {

    private static void sortHuman(Human[] humans) {
        Arrays.sort(humans);

        for (Human printHuman : humans) {
            System.out.printf(printHuman.getName() + " " + "%.1f", printHuman.getWeight());
            System.out.println();
        }
    }

    public static void main(String[] args) {

        Human[] humans = new Human[10];

        for (int i = 0; i < humans.length; i++) {
            humans[i] = new Human();
            humans[i].setName("Human" + i);
            humans[i].setWeight();
        }

        sortHuman(humans);
    }
}
