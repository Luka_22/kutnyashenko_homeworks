package Figure;

public class Main {
    public static void main(String[] args) {

        Ellipse ellipse = new Ellipse(5, 5);
        Circle circle = new Circle(5);
        Rectangle rectangle = new Rectangle(10, 10);
        Square square = new Square(10);

        System.out.println(ellipse.getPerimeter());
        System.out.println(circle.getPerimeter());
        System.out.println(rectangle.getPerimeter());
        System.out.println(square.getPerimeter());
    }
}
