package Figure;

public class Circle extends Ellipse {
    public Circle(double radius1) {
        super(radius1, radius1);
    }

    @Override
    protected double getPerimeter() {
        return 2 * Math.PI * getRadius1();
    }
}
