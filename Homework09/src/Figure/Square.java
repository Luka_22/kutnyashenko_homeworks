package Figure;

public class Square extends Rectangle {

    public Square(double side1) {
        super(side1, side1);
    }

    @Override
    protected double getPerimeter() {
        return 4 * getSide1();
    }
}
