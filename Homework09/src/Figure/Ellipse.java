package Figure;

public class Ellipse extends Figure {

    private double radius1;
    private double radius2;

    public Ellipse(double radius1, double radius2) {
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    public void setRadius1(double radius1) {
        this.radius1 = radius1;
    }

    public void setRadius2(double radius2) {
        this.radius2 = radius2;
    }

    public double getRadius1() {
        return radius1;
    }

    public double getRadius2() {
        return radius2;
    }

    @Override
    protected double getPerimeter() {
        return ((4 * Math.PI * getRadius1() * getRadius2()) + Math.pow((getRadius1() - getRadius2()), 2)) / (getRadius1() + getRadius2());
    }
}
